# variables
variable "access_key" {
  description = "access key for aws"
}
variable "secret_key" {
  description = "secret key for aws"
}
variable "instance_type" {
  default = "t3.micro"
}
variable "ami" {
  default = "ami-00f8150e6dbe1d64e" # ubuntu 22.04
}
variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKGdd7bpWs8xDsHFDLwnDpPuMvXCx1sRlBLBVI8F1cBfMGSEX/wHuuNNb/NYRqtgO+ivJd50AZVgyr+tWK57vF+OPKJp0NPYF+qXqvmXX1GG3UnoPd3isfmCzU1hGodo4i7KLaYBDx/Bdf97E/LrbqTG9DsuePGDo4J2MgXvthgSBqRXCWcjHLrPhLhtMCmTH02BBIRAMSOMx/CCF8a4YjiPyKC7on8DF0+pfpUfdgGEb7C7ppESmE2Y81zeNduv7SVfT/Zz+7Ty3kWE6A+C+jnEQXrL4PvCwHKLSYwCpegkWhpeDgvsfhiAlk7xKxt/Z3PCJBzOXwMzSoO9mD0d85nfpqY6xpsq6yR8kU7r2JmIiSWQUNrJMdXA/nJ8lN1UDdKef5JebCfteD4Suo8lSZ1EF3uE3m/pXcfLXgMCylygFQPtvyCw1vfN9T484fdbOkzNOoGUVFCEDL7UrWwCaaNfgZ028k1ByJ1RhP1XHK0mQ2CF+fsOk26v6QxUO0OLuvsVS9+1+wSsggypwO8k9Apk/iBQ+BNQxkM+E3Lc10zFVgzr2mY9AZ5uVNyjNk+lS6SBrUFF1PgcR3VHNm+DRxKAN/u/epGpGK4FgehRmrGbhWv5lERAXKOnSD8D0NlZpFdeB1yg0WXN9ONz33F+I917XiSkNFWUuQgekiXKd5cQ=="
}