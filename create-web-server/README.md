# Test Make a Web Server Use Automation Method.

I use 2 popular devops tools like Terraform and Ansible.
1. Terraform use for provisioning infrastructure, in my case i use AWS.
2. Ansible use for configuration management on that's server on cloud.

For web server application, i use nginx docker image.
And then i deploy that image on both server.

### Result
#### AWS EC2 Instances Info
![instances info](create-web-server/source/images/instances-info.png "instances-info")

#### First Server Test
![1st server](create-web-server/source/images/first-server-test.png "first-server-test")

#### Second Server Test
![2nd server](create-web-server/source/images/second-server-test.png "second-server-test")
