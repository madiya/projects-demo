provider "aws" {
  region     = "ap-southeast-3" # Jakarta, Indonesia
  access_key = var.access_key
  secret_key = var.secret_key
}

# create vpc
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main-vpc"
  }
}

# create subnet
resource "aws_subnet" "main" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true # instances will get public ip if attached to this subnet

  tags = {
    Name = "main-subnet"
  }
}

#create gateway for access internet
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main-gw"
  }
}

# create route table
resource "aws_route_table" "rb" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "main-rb"
  }
}

# associate route table with subnet
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.rb.id
}

# assign rsa 4096 pubkey for ssh
resource "aws_key_pair" "key" {
  key_name   = "ubuntu-ec2-key"
  public_key = var.public_key
}

# create security group for instances
resource "aws_security_group" "sg" {
  name        = "allow_inbound"
  description = "List allowed inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # its mean all protocol
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg"
  }
}

# create ec2 instances
resource "aws_instance" "first-node" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = aws_subnet.main.id
  key_name        = aws_key_pair.key.key_name
  security_groups = [aws_security_group.sg.id]

  tags = {
    Name = "first-server"
  }
}
resource "aws_instance" "second-node" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = aws_subnet.main.id
  key_name        = aws_key_pair.key.key_name
  security_groups = [aws_security_group.sg.id]

  tags = {
    Name = "second-server"
  }
}
