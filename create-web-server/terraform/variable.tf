# variables
variable "access_key" {
  description = "access key for aws"
}
variable "secret_key" {
  description = "secret key for aws"
}
variable "instance_type" {
  default = "t3.micro"
}
variable "ami" {
  default = "ami-00f8150e6dbe1d64e" # ubuntu 22.04
}
variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDmv/W2aUd0hiOaXvdNFPP1WwA4JurcPESixH4+JzCEAZaKJFha5R1N/rEk6Ekok3BBpEGNIk6va0stfXmLvz86ishrea+F3H6n1/t8qtsrgFrS3N+li9yhh2D7QmK2OoZFuOdhQpLZuu/99TgDsLa3Qrk+LztCIwn3fJXxQFjeSmg1N58/DQNZVcnaihMEIIc4HoZDNvLcy9LQkj5hN/VXVbHPvEsTir6m3YRNDb1Fs1NqCfob631mxFJPhkiXkZXxNV25YxpShTtsb45PbSn7kzIAgC/MeZ4m2DKJUgKFU6BF1l9FZfLTfV/Vy4rsThvy2yv9Z82KhOE86j3ZrytjxSz4JhjNPJYdtkNT6JS3WE8TRIzVAFLb3KYo1uOskKj916/1R+8uWrlKtElOuKfHklpCka/eGQeJiqx82563hqaSOeIFQd4ixCCk0LGulUM8Y+ylYBheCy9Ok8iExEEBlfoTjibWx6LYd8ZgZDwICqo2VXUSuLjprWVw/UBw8nnA7CiJhUzrNN6wmj60ziLRsjR/QTfKYqVOxZb+RyiV8y3MJRWg+6QZmihWJqNJhJveWrbPHCAJBv1M38u0Cu4Mr/ofuDMy4obN3PMWju6b2YegL6n3z4lsC5iw5xO49y89HEOUSQM6pUw4EB55chtu9x/GXIzNa5PwTaR9284XVw=="
}
